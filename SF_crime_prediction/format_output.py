import pandas as pd

inFile = "SF_prediction.csv"
formatFile = "sampleSubmission.csv"
outFile = "SF_submission.csv"

with open(formatFile, 'r') as f:
	headersOut = f.readline().strip().split(',')

predictions = pd.read_csv(inFile)
predictions['Id'] = predictions['tag']
predictions.drop('tag', axis=1, inplace=True)
predictions['max'] = predictions.ix[:,0:38].apply(max, axis=1)

for category in headersOut:
	if category <> 'Id':
		predictions[category] = (predictions[category] == predictions['max']).astype('int')
predictions.drop('max', axis=1, inplace=True)

print(predictions[headersOut].head())

predictions[headersOut].to_csv(outFile, index=False)
