import pandas as pd
from string import *

setting = "test"  # 'test' or 'train'

if setting == "train":
	inFile = "train_shuffled.csv"
	outFile = "SF_train_shuffled.csv"
	outVars = ['Id', 'year', 'month', 'hour', 'DayOfWeek', 'PdDistrict', 'Address', 'X', 'Y', 'Category']
	
if setting == "test":
	inFile = "test.csv"
	outFile = "SF_test.csv"
	outVars = ['Id', 'year', 'month', 'hour', 'DayOfWeek', 'PdDistrict', 'Address', 'X', 'Y']

inDF = pd.read_csv(inFile)

## deal with Dates
inDF['Date'] = inDF['Dates'].apply(lambda x: x.split(' ')[0])
inDF['Time'] = inDF['Dates'].apply(lambda x: x.split(' ')[1])
inDF['hour'] = inDF['Time'].apply(lambda x: x[0:2])
inDF['year'] = inDF['Date'].apply(lambda x: x[0:4])
inDF['month'] = inDF['Date'].apply(lambda x: x[5:7])

if setting == "train":
	# put in dummy IDs
	inDF['Id'] = range(1000000, 1000000 + len(inDF))

outDF = inDF[outVars]

print(outDF.head())

outDF.to_csv(outFile, index=False)
