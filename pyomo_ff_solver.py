from __future__ import division
import pandas as pd
from pyomo.environ import *
from pyomo.opt import SolverFactory
 
## IMPORT DATA ##
fileName = "ff_data.csv"
df = pd.read_csv(fileName)
 
## SETTINGS ##
max_salary = 50000
 
## DATA PREP ##
POSITIONS = ['QB', 'RB', 'WR', 'TE', 'D', 'K']
psn_limits = {'QB': 1, 'RB': 2, 'WR': 2, 'TE': 2, 'D': 1, 'K': 1}
PLAYERS = list(set(df['name']))
proj = df.set_index(['name'])['proj_pts'].to_dict()
cost = df.set_index(['name'])['cost'].to_dict()
pos = df.set_index(['name'])['position'].to_dict()
 
## DEFINE MODEL ##
model = ConcreteModel()
 
# decision variable
model.x = Var(PLAYERS, domain=Boolean, initialize=0)
 
# constraint: salary cap
def constraint_cap_rule(model):
 salary = sum(model.x[p] * cost[p] for p in PLAYERS)
 return salary <= max_salary
model.constraint_cap = Constraint(rule=constraint_cap_rule)
 
# constraint: positional limits
def constraint_position_rule(model, psn):
 psn_count = sum(model.x[p] for p in PLAYERS if pos[p] == psn)
 return psn_count == psn_limits[psn]
model.constraint_position = Constraint(POSITIONS, rule=constraint_position_rule)
 
# objective function: maximize projected points
def obj_expression(model):
 return summation(model.x, proj, index=PLAYERS)
model.OBJ = Objective(rule=obj_expression, sense=maximize)
 
# good for debugging the model
#model.pprint()
 
## SOLVE ##
opt = SolverFactory('glpk')
 
# create model instance, solve
instance = model.create()
results = opt.solve(instance)
instance.load(results) #load results back into model framework
 
## REPORT ##
print("status=" + str(results.Solution.Status))
print("solution=" + str(results.Solution.Objective.x1.value) + "\n")
print("*******optimal roster********")
P = [p for p in PLAYERS if instance.x[p].value==1]
for p in P:
 print(p + "\t" + pos[p] + "\t cost=" + str(cost[p]) + "\t projection=" + str(proj[p]))
print("roster cost=" + str(sum(cost[p] for p in P)))
